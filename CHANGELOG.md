# Version 0.4
*(24/11/2018)*

## API

### Add support for `default` and `factory` class attribute for better instantiation
### `CheckType`s constructors never return an instance of themselves, but an instance
of one of their bases if possible
### `CheckType.descriptor()` renamed into `CheckType.as_descriptor()`
### `Validator.type` renamed into `Validate.checktype`

## Aesthetic

### Better error messages on bad instantiation attempts

## Doc

### Add slight explanations about `predicate()` in the `README`
### Add `default` and `factory` specification in the `README`


# Version 0.3.0
*(22/11/2018)*

### API

## Usage as descriptor is now `MyChecktype.descriptor()`
