```python
>>> from checktypes import CheckType, checktype, checktyped

```

#### `SizedLimited`, `StringLimited` and `PhoneNumber`

```python
>>> from collections.abc import Sized
>>> class SizedLimited(Sized, CheckType):
...     maxsize = float('inf')
...     predicate = classmethod(lambda cls, s: len(s) <= cls.maxsize)
...
>>> class StringLimited32(SizedLimited):
...     maxsize = 32
...
>>> isinstance('a', StringLimited32)
True
>>> isinstance('abcdefghijklmnopqrstuvwxyz', StringLimited32)
True
>>> isinstance(''.join(['a' for i in range(35)]), StringLimited32)
False
>>> class StringLimited16(StringLimited32):
...     maxsize = 16
...
>>> import re
>>> class PhoneNumber(str, StringLimited16):
...     regex = re.compile(r'^\+?1?\d*$')
...     @classmethod
...     def predicate(cls, pn):
...         return cls.regex.match(pn)
...
>>> isinstance('0033102030405', PhoneNumber)
True
>>> isinstance('+99701020304050', PhoneNumber)
True
>>> isinstance('+997010203ABCDE', PhoneNumber)
False
>>> isinstance('+99701020304050102030405', PhoneNumber)
False

```

Inheriting from an `ABC` is often the best choice because it honors duck typing.
Defining `predicate()` as a class method depending on a class attribute is also a
very nice way to prepare subclassing and well designed class hierarchies.

`predicate()` can return a value aimed to be evaluated as boolean instead of an
actual boolean.

### Get a safe predicate to feed callables like `filter`

```python
>>> numbers = [
...     '0033102030405',
...     '+99701020304050',
...     '+997010203ABCDE',
...     '+99701020304050102030405',
... ]
>>> is_phone_number = PhoneNumber.__instancecheck__
>>> list(filter(is_phone_number, numbers))
['0033102030405', '+99701020304050']
>>> # Or directly:
... list(filter(PhoneNumber.__instancecheck__, numbers))
['0033102030405', '+99701020304050']

```

#### Classes with lay-out conflicts (impossible to inherit from them together)

```python
>>> class IntOrFloat(CheckType):
...     @classmethod
...     def extra_validation(cls, val):
...         if not isinstance(val, (int, float)):
...             raise TypeError(cls.get_errmsg(type(val)))
...
>>> isinstance(3, IntOrFloat)
True
>>> isinstance(3.14, IntOrFloat)
True
>>> isinstance('a', IntOrFloat)
False
>>> IntOrFloat.validate(3)
>>> IntOrFloat.validate(3.14)
>>> IntOrFloat.validate('a')
Traceback (most recent call last):
 ...
TypeError: expected 'IntOrFloat' but got 'str'

```

Inheriting from a suitable `ABC` if possible is better.  
In the example above, subclassing `numbers.Real` is preferred.

```python
>>> from numbers import Real
>>> class RealNumber(Real, CheckType):
...     pass
>>> isinstance(3, RealNumber)
True
>>> isinstance(3.14, RealNumber)
True
>>> isinstance('a', RealNumber)
False
>>> RealNumber.validate(3)
>>> RealNumber.validate(3.14)
>>> RealNumber.validate('a')
Traceback (most recent call last):
 ...
TypeError: expected 'RealNumber' but got 'str'

```
#### With `dataclasses`

```python
>>> from dataclasses import dataclass
>>> @checktyped
... @dataclass
... class Point2D:
...     x: int
...     y: int
...
>>> p = Point2D()
Traceback (most recent call last):
 ...
TypeError: __init__() missing 2 required positional arguments: 'x' and 'y'
>>> p = Point2D(3, 4)
>>> p
Point2D(x=3, y=4)
>>> p.x = 'a'
Traceback (most recent call last):
 ...
TypeError: expected 'int' but got 'str' for 'x' attribute of 'Point2D' object

```

Be careful to use `checktyped` as the outer-most decorator in this case.

#### Silent a picky linter (such as `pylint`) about lambda assignation

Use `staticmethod`.

```python
>>> class PositiveInteger(int, CheckType):
...     predicate = staticmethod(lambda n: n > 0)
...

```

### `mypy` compliance

Use `typing.cast`.

```python
>>> class Contact:
...     def __init__(self, name: StringLimited32, pn: PhoneNumber) -> None:
...         self.name = name
...         self.pn = pn
...
>>> from typing import cast
>>> c = Contact(cast(StringLimited32, 'European Emergency Number'),
...             cast(PhoneNumber, 112))

```

