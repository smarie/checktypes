"""CheckType classes utility module.

This module contains:

* CheckType   : superclass to be inherited from (object oriented API)
* checktype   : factory building a CheckType subclass (functional API)
* checktyped  : class decorator adding checktype descriptors

A CheckType is a type providing a custom behaviour when passed in the
second argument of isinstance(): it will return True if it first
argument satisfies a predicate and False otherwise. This predicate is a
class callable named predicate.

If a CheckType inherits from other classes, it will check first if the
passed object is an instance of all of them.

A CheckType can also be also be instantiated and used as a data
descriptor using the as_as_descriptor() alternative constructorinside a
class body: it returns an instance of the Validator class, wich is a
descriptor, and an exception will be raised when trying to assign a
value that doesn't pass the isinstance() check. See CheckType.validate
documentation.

A value can be converted into a CheckType if it passes the isinstance()
check.
"""
from __future__ import print_function

import sys
from abc import ABCMeta

from six import with_metaclass, raise_from
try:
    from typing import get_type_hints
except ImportError:
    # no type hints :)
    def get_type_hints(cls):
        return ()
try:
    from reprlib import repr
except ImportError:
    import repr as reprlib
from .validator import Validator


__all__ = ["CheckType", "checktype", "checktyped"]


PY2 = sys.version_info < (3, 0)


def _repr(obj):
    return repr(obj.__name__) if isinstance(obj, type) else repr(obj)


class _MetaCheckType(ABCMeta):
    """Metaclass for CheckTypes.

    It inherits from ABCMeta to avoid metaclass conflicts.
    """

    def __new__(mcls, name, bases, ns):
        if PY2:
            try:
                predicate = ns['predicate']
                if not isinstance(predicate, staticmethod) and not isinstance(predicate, classmethod):
                    # transform callables to static methods
                    ns['predicate'] = staticmethod(predicate)
            except KeyError:
                pass

        cls = super(_MetaCheckType, mcls).__new__(mcls, name, bases, ns)
        cls._checktype_registry = list()
        doc = cls.__doc__
        clsname = cls.__name__
        expected = "%s (%s)" % (repr(clsname), doc) if doc else "%s" % repr(clsname)
        cls._expected = expected
        cls._errmsg1 = "expected {expected} but got ".format(expected=expected) + "{}"
        cls.__repr__ = cls.__class__._repr
        return cls

    def __init__(cls, name, bases, ns):
        if hasattr(cls, "default"):
            default = cls.default
            if not isinstance(default, cls):
                msg = "{defrepr} is not an instance of {clsrepr}".format(defrepr=_repr(default), clsrepr=_repr(cls))
                raise ValueError(msg)

    def __call__(cls, *args, **kwargs):
        if cls is CheckType:
            raise TypeError("'CheckType' can't be instantiated")
        elif not args and not kwargs and hasattr(cls, "default"):
            obj = cls.default
        elif hasattr(cls, "factory"):
            obj = cls.factory(*args, **kwargs)
        else:
            obj = cls._instantiate_from_base(*args, **kwargs)
        cls._validate_cast(obj)
        return obj

    def _instantiate_from_base(cls, *args, **kwargs):
        err = None
        rmro = reversed(cls.mro()[1:-2])  # Excluding cls, object and CheckType
        for base in rmro:
            try:
                obj = base(*args, **kwargs)
            except Exception as exc:
                if err is None:
                    err = exc
            else:
                return obj
        raise err

    def _validate_cast(cls, obj):
        try:
            cls.validate(obj)
        except Exception as err:
            if isinstance(err, TypeError):
                errtype = TypeError
                # python 2 compat type(obj) > obj.__class__
                objrepr = _repr(obj.__class__) + " object"
            else:
                # python 2 compat type(err) > err.__class__
                errtype = err.__class__
                objrepr = _repr(obj)
            msg = "{objrepr} cannot be interpreted as a {expected}".format(objrepr=objrepr, expected=cls._expected)
            raise_from(errtype(msg), err)

    def __instancecheck__(cls, instance):
        try:
            cls._CheckType__validate(instance)
        except:  # Any kind of exception is interpreted as a failure
            if not cls._check_registries(instance):
                return False
        return True

    def _check_registries(cls, instance):
        # Recursive helper method checking the registry of a CheckType
        # and of all of its subclasses.
        if cls is CheckType:
            return True
        if instance in cls._checktype_registry:
            return True
        for subcls in cls.__subclasses__():
            if subcls._check_registries(instance):
                # Cache the instance in the registry of the original class if
                # it was found in one of its subclasses.
                cls.register(instance)
                return True
        return False

    def _dump_registry(cls, file=None):
        try:
            qname = cls.__qualname__
        except AttributeError:
            qname = cls.__name__
        print("Class: %s.%s" % (cls.__module__, qname), file=file)
        for attr in cls.__dict__:
            if attr.startswith("_checktype_"):
                print("{attr}: {attrval}".format(attr=attr, attrval=getattr(cls, attr)), file=file)

    @staticmethod
    def _repr(self):
        cls = self.__class__
        clsname = cls.__name__
        return "{clsname}({selfrepr})".format(clsname=clsname, selfrepr=cls.__base__.__repr__(self))


class CheckType(with_metaclass(_MetaCheckType, object)):
    """Special kind of type providing a type checking abstraction."""

    @classmethod
    def as_descriptor(cls, owner=None, name=None):
        """Instantiate a CheckType as a Validator descriptor.

        Args:
            owner: the class that is targeted to own the descriptor.
            name: the name to which the descriptor is aimed to be
                assigned in the namespace of the class.

        Returns:
            An instance of the Validator class.
        """
        return Validator(checktype=cls, owner=owner, name=name)

    @staticmethod
    def predicate(val):
        """Callable testing a value.

        This method is meant to be overriden by the subclasser.
        It can be any callable assigned to a class attribute, a
        staticmethod or a classmethod.
        This method is not meant to be used after the class creation,
        but other methods will leverage it for validation purpose.

        Args:
            val: any value to test.

        Returns:
            A boolean (or value aimed to be evaluated as a boolean) that
            equals True if the value passed the test, False otherwise.

        Notes:
            In the CheckType superclass, the default implementation is
            to always return True.
        """
        return True

    @classmethod
    def validate(cls, val):
        """Data validation classmethod.

        It will raise an appropriate exception according to the value
        passed as argument.

        Args:
            val: any value to test.

        Raises:
            TypeError: the value is not an instance of all cls bases.
            ValueError: the value doesn't satisfy predicate().
        """
        try:
            cls.extra_validation(val)
            if not all(isinstance(val, base) for base in cls.__bases__):
                # python 2 compat type(val) > val.__class__
                raise TypeError(cls.get_errmsg(val.__class__))

            if not cls.predicate(val):
                raise ValueError(cls.get_errmsg(val))
        except:
            if not cls._check_registries(val):
                raise

    __validate = validate  # Prevent from override in subclasses

    @classmethod
    def extra_validation(cls, val):
        """Extra validation class method called by validate.

        This method is meant to be override in subclasses in order to
        perform extra validation or raise different kind of exceptions.
        If a TypeError or a ValueError is raised, the error message will
        be handled automatically, unless one is already given.

        Args:
            val: any value to test.

        Raises:
            up to the subclasser...
        """
        pass

    @classmethod
    def register(cls, instance):
        """Register a virtual instance of a CheckType.

        This classmethod can be used to force some value to pass the
        isinstance() and validate() tests.
        Be aware that a value passed in the registry of a subclass will
        also pass the tests of all of its superclasses.

        Args:
            instance: any value.
        """
        if instance not in cls._checktype_registry:
            cls._checktype_registry.append(instance)

    @classmethod
    def get_errmsg(cls, but_got):
        """Get an preformatted error message from a value.

        Args:
            but_got: non expected received value (if it's a class its
                name will be used).

        Returns:
            An error message as a string that looks like:
            "expected '<SomeClass>' but got <some_val>"
        """
        if isinstance(but_got, type):
            but_got = but_got.__name__
        return cls._errmsg1.format(repr(but_got))


def checktype(name, bases=None, predicate=None, doc=None, **kwargs):
    """Factory function returning a CheckType subclass.

    Args:
        name: the name of the class to be returned.
        bases: the class or tuple of classes to be inherited from.
        predicate: a callable, staticmethod or classmethod accepting one
            and only argument and returning a boolean.
        doc: the __doc__ of the class to be returned.
        kwargs: attributes or methods to be injected in the namespace of
            the class.

    Returns:
        A subclass of CheckType.
    """
    if bases is None:
        bases = (CheckType,)
    elif isinstance(bases, tuple):
        bases = bases + (CheckType, )
    elif isinstance(bases, type):
        bases = (bases, CheckType)
    namespace = {"__doc__": doc}
    namespace.update(kwargs)
    if predicate:
        namespace["predicate"] = predicate
    return type(name, bases, namespace)


def checktyped(cls):
    """Decorator for injecting validators in a class.

    It uses the special the type hints of the class if found. Every
    annotation with a non type value will be ignored, unless it can be
    evaluated as a type.

    Args:
        cls: the class to decorate.

    Returns:
        The same class with validators.
    """
    annotations = get_type_hints(cls)
    if annotations:
        for name, typ in annotations.items():
            if isinstance(typ, type):
                setattr(cls, name, Validator(typ, cls, name))
    return cls
