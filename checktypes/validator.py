from inspect import isclass
from keyword import iskeyword

from six import raise_from

__all__ = ["Validator"]


try:
    "".isidentifier()

    def isidentifier(candidate):
        return candidate.isidentifier()

except AttributeError:
    import keyword
    import re

    # from https://stackoverflow.com/questions/2544972/how-can-get-python-isidentifer-functionality-in-python-2-6
    def isidentifier(candidate):
        "Is the candidate string an identifier in Python 2.x"
        is_not_keyword = candidate not in keyword.kwlist
        pattern = re.compile(r'^[a-z_][a-z0-9_]*$', re.I)
        matches_pattern = bool(pattern.match(candidate))
        return is_not_keyword and matches_pattern


def _is_valid_name(s):
    return isinstance(s, str) and isidentifier(s) and not iskeyword(s)


class Validator(object):
    """Descriptor class performing type type checking."""

    def __init__(self, checktype=object, owner=None, name=None):
        """
        Args:
            checktype: type that wil be used for type checking
            owner: the class that is targeted to own the descriptor.
            name: the name to which the descriptor is aimed to be
                assigned in the namespace of the class.
        """
        self.checktype = checktype
        if owner or name:
            self._check_owner_and_name(owner, name)
            self.__set_name__(owner, name)

    def _check_owner_and_name(self, owner, name):
        if not isclass(owner):
            raise TypeError("'owner' must be a class")
        if not _is_valid_name(name):
            raise TypeError("'name' must be a valid identifier")

    def __set_name__(self, owner, name):
        """ For python 3.6+ this will be called automatically """
        self.owner = owner
        self.name = name
        self._name = "_{name}".format(name=name)
        self._errmsg1 = "expected {reprname} but got ".format(reprname=repr(self.checktype.__name__)) + "{}"
        self._errmsg2 = " for {selfname} attribute of ".format(selfname=repr(self.name)) + "{} object"

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self
        try:
            return getattr(obj, self._name)
        except AttributeError as e:
            # python 2 compat: type(e) -> e.__class__
            raise_from(e.__class__(str(e).replace(self._name, self.name)), None)

    def __set__(self, obj, val):
        if not isinstance(val, self.checktype):
            # python 2 compat: type(obj) -> obj.__class__
            errmsg2 = self._errmsg2.format(repr(obj.__class__.__name__))
            try:
                self.checktype.validate(val)
            except AttributeError:
                # python 2 compat: type(val) -> val.__class__
                errmsg1 = self._errmsg1.format(repr(val.__class__.__name__))
                raise TypeError(errmsg1 + errmsg2)
            except (TypeError, ValueError) as err:
                # python 2 compat: type(err) -> err.__class__
                raise_from(err.__class__(str(err) + errmsg2), None)
        setattr(obj, self._name, val)

    def __repr__(self):
        cls = self.__class__.__name__
        descr = self.name
        owner = self.owner.__name__
        checktype = self.checktype.__name__
        return "{cls}(checktype={checktype}, owner={owner}, name={descr})".format(cls=cls, checktype=checktype,
                                                                                    owner=owner, descr=repr(descr))
