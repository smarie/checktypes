from collections import Container
import pytest
from checktypes import checktype, CheckType, checktyped
from base_test import make_CheckType, PositiveInt, SubInteger


def test_TinyInt_instantiation():
    class CustomInt(int, CheckType):
        maxval = None
        minval = None
        @classmethod
        def predicate(cls, n):
            if cls.maxval is not None and n > cls.maxval:
                return False
            if cls.minval is not None and n < cls.minval:
                return False
            return True
    class TinyInt(CustomInt):
        minval = 0
        maxval = 255
    n = CustomInt(5)
    assert type(n) is int


def test_default_value():
    class MyContainer(Container, CheckType):
        default = []
    assert MyContainer() == []


def test_factory():
    class MyContainer(Container, CheckType):
        factory = tuple
    assert MyContainer() == ()
    assert MyContainer(range(3)) == (0, 1, 2)
    assert MyContainer([3, 4, 5]) == (3, 4, 5)


def test_bad_default_value_error():
    with pytest.raises(ValueError) as err:
        class MyContainer(Container, CheckType):
            default = 0
    assert "0 is not an instance of 'MyContainer'" in str(err)


def test_default_and_factory():
    class MyContainer(Container, CheckType):
        default = [None, None, None]
        predicate = lambda self: len(self) == 3
        factory = list
    assert MyContainer() == [None, None, None]
    assert MyContainer(range(3)) == [0, 1, 2]
    assert MyContainer((3, 4, 5)) == [3, 4, 5]


def test_conversion_SubInteger(SubInteger):
    x = 0
    for val in [-1, 0, 1, 42, '0', 3.14]:
        x = SubInteger(val)
        assert type(x) is int


def test_no_conversion_SubInteger(SubInteger):
    x = 0
    for val in ['', 'a', [], (), {}]:
        with pytest.raises((TypeError, ValueError)) as e:
            x = SubInteger(val)


def test_conversion_PositiveInt(PositiveInt):
    x = 0
    for val in [1, 42, '1', 3.14]:
        x = PositiveInt(val)
        assert type(x) is int


def test_no_conversion_PositiveInt(PositiveInt):
    x = 0
    for val in [-1, 0, '0', 0.4]:
        with pytest.raises(ValueError):
            x = PositiveInt(val)


def test_no_int_conversion_PositiveInt(PositiveInt):
    for val in [(), [], {}]:
        with pytest.raises(TypeError):
            x = PositiveInt(val)


def test_inheriting_ABC_without_implementing_abstract_methods():
    class MyC(Container):
        pass
    with pytest.raises(TypeError) as err:
        MyC()
    exc = None
    try:
        Container()
    except TypeError as e:
        exc = e
    assert str(exc).replace('Container', '') in str(err).replace('MyC', '')
