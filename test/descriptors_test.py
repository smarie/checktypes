import sys

import pytest
from collections import Sized
from checktypes import CheckType, checktype, checktyped, Validator
from base_test import make_CheckType, PositiveInt


PY35 = sys.version_info < (3, 6)


@pytest.fixture(params=['xindict', 'xinslot', 'mix_xindict', 'mix_xinslot'])
def ClassNoDescr(request):
    if request.param == 'xindict':
        class C(object):
            pass
        return C
    if request.param == 'xinslot':
        class C(object):
            __slots__ = '_x',
        return C
    if request.param == 'mix_xindict':
        class C(object):
            __slots__ = '_x', '__dict__'
        return C
    if request.param == 'mix_xinslot':
        class C(object):
            __slots__ = '_y', '__dict__'
        return C


@pytest.fixture(params=['xindict', 'xinslot', 'mix_xindict', 'mix_xinslot'])
def ClassWithDescr_x(request):
    if request.param == 'xindict':
        class C(object):
            x = CheckType.as_descriptor()

        if PY35:
            C.__dict__['x'].__set_name__(C, 'x')
        return C
    if request.param == 'xinslot':
        class C(object):
            __slots__ = '_x',
            x = CheckType.as_descriptor()

        if PY35:
            C.__dict__['x'].__set_name__(C, 'x')
        return C
    if request.param == 'mix_xindict':
        class C(object):
            __slots__ = '_x', '__dict__'
            x = CheckType.as_descriptor()

        if PY35:
            C.__dict__['x'].__set_name__(C, 'x')
        return C
    if request.param == 'mix_xinslot':
        class C(object):
            __slots__ = '_y', '__dict__'
            x = CheckType.as_descriptor()

        if PY35:
            C.__dict__['x'].__set_name__(C, 'x')
        return C


def test_instantiation(ClassWithDescr_x):
    assert isinstance(ClassWithDescr_x.x, CheckType)


def test_basic_behaviour(ClassWithDescr_x):
    c = ClassWithDescr_x()
    c.x = 0
    for val in [0, 'a']:
        c.x = val
        assert c.x == val
        assert c._x == val
    with pytest.raises(AttributeError):
        del c.x


def test_instantiation_as_descriptor(ClassNoDescr):
    v = CheckType.as_descriptor(ClassNoDescr, 'x')
    assert isinstance(v, CheckType)


def test_basic_behaviour_as_descriptor(ClassNoDescr):
    v = CheckType.as_descriptor(ClassNoDescr, 'x')
    c = ClassNoDescr()
    v.__set__(c, 0)
    assert v.__get__(c) == 0
    v.__set__(c, 'a')
    assert v.__get__(c) == 'a'
    with pytest.raises(AttributeError):
        v.__del__(c)


def test_basic_behaviour_descriptor_after_assign(ClassNoDescr):
    v = CheckType.as_descriptor(ClassNoDescr, 'x')
    ClassNoDescr.x = v
    test_basic_behaviour(ClassNoDescr)


def test_basic_behaviour_descriptor_after_assign_to_other_class(ClassNoDescr):
    v = CheckType.as_descriptor(ClassNoDescr, 'x')
    class C(object):
        pass
    C.x = v
    test_basic_behaviour(C)


def test_instantiation_descriptor_bad_type_argmunents():
    class C(object):
        pass
    errmsg = "'owner' must be a class"
    for bad_owner in [0, '', (), None]:
        with pytest.raises(TypeError, match=errmsg):
            x = CheckType.as_descriptor(bad_owner, 'x')
    errmsg = "'name' must be a valid identifier"
    for bad_name in [0, (), None, '^asdf', 'asdf asdf', 'class']:
        with pytest.raises(TypeError, match=errmsg):
            x = CheckType.as_descriptor(C, bad_name)


def test_repr(make_CheckType):
    class C(object):
        x = CheckType.as_descriptor()

    if PY35:
        C.__dict__['x'].__set_name__(C, 'x')
    clsname = C.x.__class__.__name__
    assert repr(C.x) == "Validator(checktype=CheckType, owner=C, name='x')"
    class D(object):
        y = CheckType.as_descriptor()

    if PY35:
        D.__dict__['y'].__set_name__(D, 'y')
    assert repr(D.y) == "Validator(checktype=CheckType, owner=D, name='y')"
    class E(object):
        pass
    z = CheckType.as_descriptor(E, 'z')
    assert repr(z) == "Validator(checktype=CheckType, owner=E, name='z')"
    C = make_CheckType('C', int)
    n = C.as_descriptor(E, 'n')
    assert repr(n) == "Validator(checktype=C, owner=E, name='n')"


def test_error_messages(PositiveInt):
    class C(object):
        x = CheckType.as_descriptor()
    if PY35:
        C.__dict__['x'].__set_name__(C, 'x')
    errmsg = "'C' object has no attribute 'x'"
    with pytest.raises(AttributeError, match=errmsg):
        C().x
    class C(object):
        x = PositiveInt.as_descriptor()
    if PY35:
        C.__dict__['x'].__set_name__(C, 'x')
    c = C()
    m = "expected 'PositiveInt' but got 'str' for 'x' attribute of 'C' object"
    with pytest.raises(TypeError, match=m):
        c.x = 'a'
    m = "expected 'PositiveInt' but got -1 for 'x' attribute of 'C' object"
    with pytest.raises(ValueError, match=m):
        c.x = -1
    class D(C):
        pass
    d = D()
    m = "expected 'PositiveInt' but got 'str' for 'x' attribute of 'D' object"
    with pytest.raises(TypeError, match=m):
        d.x = 'a'
    m = "expected 'PositiveInt' but got -1 for 'x' attribute of 'D' object"
    with pytest.raises(ValueError, match=m):
        d.x = -1


@pytest.mark.skipif(sys.version_info < (3, 7), reason="Attributes annotations not supported before python 3.7")
def test_decorator(PositiveInt):
    from ._pep484_annotations import create_for_test_decorator
    C = create_for_test_decorator(PositiveInt)

    # add @checktyped manually
    C = checktyped(C)

    assert isinstance(C.x, Validator)
    assert C.x.owner is C
    assert C.x.name is 'x'
    assert repr(C.x) == "Validator(checktype=PositiveInt, owner=C, name='x')"
    assert isinstance(C.y, Validator)
    assert C.y.owner is C
    assert C.y.name is 'y'
    assert repr(C.y) == "Validator(checktype=int, owner=C, name='y')"
    msg = "expected 'int' but got 'str' for 'y' attribute of 'C' object"
    with pytest.raises(TypeError, match=msg):
        C().y = 'a'


def test_inherit_ABC_no_abstractmethod_override(make_CheckType):
    C = make_CheckType('C', Sized)
    class C(object):
        x = C.as_descriptor()
    if PY35:
        C.__dict__['x'].__set_name__(C, 'x')
    c = C()
    c.x = []
    with pytest.raises(TypeError):
        c.x = 3


def test_validator_with_classic_class():
    class C(object):
        x = Validator(checktype=int)

    if PY35:
        C.__dict__['x'].__set_name__(C, 'x')
    c = C()
    c.x = 3
    msg = "expected 'int' but got 'str' for 'x' attribute of 'C' object"
    with pytest.raises(TypeError, match=msg):
        c.x = 'a'
