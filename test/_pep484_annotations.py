def create_for_test_decorator(PositiveInt):
    class C:
        x: PositiveInt
        y: int

    return C
