import sys

import pytest
from numbers import Real
from abc import ABCMeta
from collections import Sized, Mapping
from checktypes import checktype, CheckType, checktyped


PY2 = sys.version_info < (3, 0)


def make_CheckType_from_inheritance(name, typ, predicate=None, **kwds):
    if predicate:
        if not PY2:
            kwds["predicate"] = predicate
        else:
            # transform callables to static methods
            if not isinstance(predicate, staticmethod) and not isinstance(predicate, classmethod):
                kwds["predicate"] = staticmethod(predicate)
            else:
                kwds["predicate"] = predicate
    return type(name, (typ, CheckType), kwds)


def make_CheckType_from_factory(name, typ, predicate=None, **kwds):
    return checktype(name, typ, predicate, **kwds)


@pytest.fixture(params=['inheritance', 'factory'])
def make_CheckType(request):
    if request.param == 'inheritance':
        return make_CheckType_from_inheritance
    elif request.param == 'factory':
        return make_CheckType_from_factory


@pytest.fixture
def SubInteger(make_CheckType):
    name = 'SubInteger'
    predicate = lambda n: True
    return make_CheckType(name, int, predicate)


@pytest.fixture
def PositiveInt(make_CheckType):
    name = 'PositiveInt'
    predicate = lambda n: n > 0
    return make_CheckType(name, int, predicate)


@pytest.fixture
def LimitedString16(make_CheckType):
    name = 'LimitedString16'
    predicate = lambda s: len(s) <= 16
    return make_CheckType(name, str, predicate)


def test_isinstance():
    for val in (1, 'a', [3, 4, 5]):
        assert isinstance(val, CheckType)


def test_validate():
    for val in (1, 'a', [3, 4, 5]):
        CheckType.validate(val)


def test_isinstance_SubInteger(SubInteger):
    for val in [-1, 0, 1, 42]:
        assert isinstance(val, SubInteger)


def test_not_isinstance_SubInteger(SubInteger):
    for val in ['a', '1', 3.14, [], (), {}]:
        assert not isinstance(val, SubInteger)


def test_subclass_isinstance(PositiveInt, LimitedString16):
    assert isinstance(1, PositiveInt)
    assert not isinstance(-1, PositiveInt)
    assert not isinstance('abc', PositiveInt)
    assert isinstance('abc', LimitedString16)
    assert not isinstance('qwertasdfgzxcvvbqwert', LimitedString16)
    assert not isinstance(1, LimitedString16)


def test_subclass_validate(PositiveInt, LimitedString16):
    PositiveInt.validate(1)
    LimitedString16.validate('abc')
    with pytest.raises(TypeError):
        PositiveInt.validate('abc')
    with pytest.raises(ValueError):
        PositiveInt.validate(-1)
    with pytest.raises(TypeError):
        LimitedString16.validate(1)
    with pytest.raises(ValueError):
        LimitedString16.validate('qwertasdfgzxcvvbqwert')


def test_issubclass_Integers(SubInteger, PositiveInt):
    assert isinstance(1, int)
    assert isinstance(1, SubInteger)
    assert isinstance(1, PositiveInt)
    assert issubclass(SubInteger, int)
    assert issubclass(PositiveInt, int)
    assert not issubclass(int, SubInteger)
    assert not issubclass(int, PositiveInt)
    assert not issubclass(SubInteger, PositiveInt)
    assert not issubclass(PositiveInt, SubInteger)


def test_isinstance_PositiveInt(PositiveInt):
    for val in [1, 42, 100]:
        assert isinstance(val, PositiveInt)


def test_not_isinstance_PositiveInt(PositiveInt):
    for val in [-1, 0, 3.14, (), [], {}]:
        assert not isinstance(val, PositiveInt)


def test_register(PositiveInt):
    PositiveInt.register(0)
    assert isinstance(0, PositiveInt)
    PositiveInt.validate(0)


def test_register_subclass(PositiveInt):
    class OddPositiveInt(PositiveInt):
        predicate = lambda n: n % 2 == 0
    OddPositiveInt.register(-2)
    assert isinstance(-2, PositiveInt)      is True
    assert isinstance(-2, OddPositiveInt)   is True
    PositiveInt.validate(-2)
    class TenthPositiveInt(OddPositiveInt):
        predicate = lambda n: n % 10 == 0
    TenthPositiveInt.register(-3)
    assert isinstance(-3, TenthPositiveInt) is True
    assert isinstance(-3, OddPositiveInt)   is True
    assert isinstance(-3, PositiveInt)      is True
    PositiveInt.validate(-3)


def test_register_bases(PositiveInt):
    class OddPositiveInt(PositiveInt):
        predicate = lambda n: n % 2 == 0
    PositiveInt.register(-1)
    assert isinstance(-1, PositiveInt)      is True
    assert isinstance(-1, OddPositiveInt)   is False
    PositiveInt.validate(-1)
    PositiveInt.register(0)
    assert isinstance(0, PositiveInt)      is True
    assert isinstance(0, OddPositiveInt)   is True
    PositiveInt.validate(0)


def test_factory_kwargs_update_namespace():
    predicate = classmethod(lambda cls, l: len(l) <= cls.maxsize)
    C = checktype('C', list, predicate, maxsize=1)
    assert isinstance([0], C)
    assert not isinstance([0, 1], C)


def test_factory_several_bases(PositiveInt):
    C = checktype('C', (PositiveInt, int), lambda n: True)
    assert isinstance(3, C)
    assert not isinstance(3.14, C)


def test_issubclass():
    C = checktype('C', Sized,
                  classmethod(lambda cls, l: len(l) <= cls.max), max=1)
    assert isinstance({0}, C)
    assert isinstance([0], C)
    assert not issubclass(list, C)
    class D(C): pass
    assert isinstance({0}, D)
    assert isinstance([0], D)
    assert not issubclass(list, D)
    assert not issubclass(C, D)
    assert issubclass(D, C)


def test_any_callable_or_staticmethod_as_pred1arg(make_CheckType):
    l = lambda x: x > 0
    sm = staticmethod(l)
    class C:
        def __call__(self, x):
            return x > 0
    for predicate in [l, sm, C(), C().__call__]:
        CT = make_CheckType('CT', int, predicate)
        assert CT.predicate(1)
        assert not CT.predicate(-1)
        assert isinstance(1, CT)
        assert not isinstance(-1, CT)
        assert not isinstance('a', CT)


def test_classmethod_as_pred2args(make_CheckType):
    predicate = classmethod(lambda cls, s: len(s) <= cls.maxsize)
    CT = make_CheckType('CT', str, predicate, maxsize=8)
    assert CT.predicate('abc')
    assert not CT.predicate('qwerqwert')
    assert isinstance('abc', CT)
    assert not isinstance('qwerqwert', CT)
    assert not isinstance(1, CT)


def test_errmsg(PositiveInt):
    errmsg = "expected 'PositiveInt' but got -1"
    with pytest.raises(ValueError, match=errmsg):
        PositiveInt.validate(-1)
    errmsg = "expected 'PositiveInt' but got 'str'"
    with pytest.raises(TypeError, match=errmsg):
        PositiveInt.validate('a')
